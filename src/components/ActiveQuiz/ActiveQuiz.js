import React from 'react'

import classes from './ActiveQuiz.module.css'
import AnswerList from './AnswerList/AnswerList'

const ActiveQuiz = props => (
  <div className={ classes.ActiveQuiz }>
    <p className={ classes.Question }>
      <span>
        <strong>{ props.aswerNumber }.</strong>&nbsp;
        { props.question }
      </span>
      <small>{ props.aswerNumber } из { props.quizLength }</small>
    </p>

    <AnswerList
      answers={ props.answers }
      onAnswerClick={ props.onAnswerClick }
      answerState={props.answerState}
    />

  </div>
)


export default ActiveQuiz